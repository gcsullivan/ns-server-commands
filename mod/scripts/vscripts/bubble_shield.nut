global function BubbleShieldCommand

struct { table<entity, entity> shield } file

void function BubbleShieldCommand()
{
	#if SERVER
	AddClientCommandCallback("bubbleshield", Command1)
	AddClientCommandCallback("bs", Command1)
	AddClientCommandCallback("unbubbleshield", Command2)
	AddClientCommandCallback("unbs", Command2)
	#endif
}

bool function Command1(entity player, array<string> args)
{
	if (PlayerIsAdmin(player))
    {
		array<entity> players = [player]
        float duration = 1

		if (args.len() > 0)
			duration = args[0].tofloat()
		if (args.len() > 1)
			players = GetSelectedPlayers(player, args.slice(1))

        foreach (entity p in players)
            thread BubbleShield(p, duration)
    }

	return true
}

bool function Command2(entity player, array<string> args)
{
	if (PlayerIsAdmin(player))
    {
        foreach (entity p in GetSelectedPlayers(player, args))
        {
            if (p in file.shield)
                thread DestroyBubbleShield(file.shield[p])
        }
    }

	return true
}

void function BubbleShield(entity player, float duration)
{
	entity bubbleShield = CreateEntity("prop_dynamic")

	bubbleShield.SetValueForModelKey($"models/fx/xo_shield.mdl")
	bubbleShield.kv.solid = SOLID_VPHYSICS
    bubbleShield.kv.rendercolor = "81 130 151"
    bubbleShield.kv.contents = (int(bubbleShield.kv.contents) | CONTENTS_NOGRAPPLE)

    vector angles = Vector(0, player.EyeAngles().y, 0)

	bubbleShield.SetOrigin(player.GetOrigin() + Vector(0, 0, 25))
	bubbleShield.SetAngles(angles)

    // Blocks bullets, projectiles but not players and not AI
	// bubbleShield.kv.CollisionGroup = TRACE_COLLISION_GROUP_BLOCK_WEAPONS

	bubbleShield.SetBlocksRadiusDamage(true)

	DispatchSpawn(bubbleShield)

    SetTeam(bubbleShield, player.GetTeam())

    array<entity> bubbleShieldFXs
	vector coloredFXOrigin = player.GetOrigin() + Vector(0, 0, 25)
	table bubbleShieldDotS = expect table(bubbleShield.s)

	if (player.GetTeam() == TEAM_UNASSIGNED)
	{
		entity neutralColoredFX = StartParticleEffectInWorld_ReturnEntity(BUBBLE_SHIELD_FX_PARTICLE_SYSTEM_INDEX, coloredFXOrigin, <0, 0, 0>)
		SetTeam(neutralColoredFX, player.GetTeam())

		bubbleShieldDotS.neutralColoredFX <- neutralColoredFX
		bubbleShieldFXs.append(neutralColoredFX)
	}
	else
	{
		// Create friendly and enemy colored particle systems
		entity friendlyColoredFX = StartParticleEffectInWorld_ReturnEntity(BUBBLE_SHIELD_FX_PARTICLE_SYSTEM_INDEX, coloredFXOrigin, <0, 0, 0>)
		SetTeam(friendlyColoredFX, player.GetTeam())
		friendlyColoredFX.kv.VisibilityFlags = ENTITY_VISIBLE_TO_FRIENDLY
		EffectSetControlPointVector(friendlyColoredFX, 1, FRIENDLY_COLOR_FX)

		entity enemyColoredFX = StartParticleEffectInWorld_ReturnEntity(BUBBLE_SHIELD_FX_PARTICLE_SYSTEM_INDEX, coloredFXOrigin, <0, 0, 0>)
		SetTeam(enemyColoredFX, player.GetTeam())
		enemyColoredFX.kv.VisibilityFlags = ENTITY_VISIBLE_TO_ENEMY
		EffectSetControlPointVector(enemyColoredFX, 1, ENEMY_COLOR_FX)

		bubbleShieldDotS.friendlyColoredFX <- friendlyColoredFX
		bubbleShieldDotS.enemyColoredFX <- enemyColoredFX
		bubbleShieldFXs.append(friendlyColoredFX)
		bubbleShieldFXs.append(enemyColoredFX)
	}

    file.shield[player] <- bubbleShield

    EmitSoundOnEntity(bubbleShield, "BubbleShield_Sustain_Loop")

    thread CleanupBubbleShield(bubbleShield, bubbleShieldFXs, duration)
}

void function CleanupBubbleShield(entity bubbleShield, array<entity> bubbleShieldFXs, float fadeTime)
{
	bubbleShield.EndSignal("OnDestroy")

	OnThreadEnd(
		function () : (bubbleShield, bubbleShieldFXs)
		{
			if (IsValid_ThisFrame(bubbleShield))
			{
				StopSoundOnEntity(bubbleShield, "BubbleShield_Sustain_Loop")
				EmitSoundOnEntity(bubbleShield, "BubbleShield_End")
				DestroyBubbleShield(bubbleShield)
			}

			foreach (fx in bubbleShieldFXs)
			{
				if (IsValid_ThisFrame(fx))
					EffectStop(fx)
			}
		}
	)

	wait fadeTime
}
