global function AutoTrollHook

void function AutoTrollHook()
{
	AddCallback_OnPlayerRespawned(Handler)
}

void function Handler(entity player)
{
	if (PlayerIsTroll(player))
    {
        thread Troll(player)
    }
}

void function Troll(entity player)
{
    wait 5

    Health([player], 2500)
}
