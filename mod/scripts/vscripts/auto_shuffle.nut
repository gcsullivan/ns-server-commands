global function AutoShuffleHook

array<string> allowedModes = [
    "tdm",						// Skirmish
    "ps", 						// Pilots vs Pilots
	"lts", 						// Last Titan Standing
	"ctf", 						// Capture the Flag
	"ttdm", 					// Titan Brawl
	"turbo_ttdm", 				// Turbo Titan Brawl
	"attdm", 					// Aegis Titan Brawl
	"lf" 						// Live Fire
]

void function AutoShuffleHook()
{
    // Shuffle teams at the end of the match
	AddCallback_GameStateEnter(eGameState.Postmatch, AutoShuffle)
}

void function AutoShuffle()
{
    bool enabled = GetConVarInt("auto_shuffle") > 0
	bool inLobby = GetMapName() == "mp_lobby"
	bool allowed = allowedModes.find(GAMETYPE) > -1

    if (!enabled || inLobby || !allowed)
        return

	ShuffleTeams()
}
