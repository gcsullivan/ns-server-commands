global function OnStartInit
global function PlayerIsAdmin
global function PlayerIsTroll

struct {
	array<string> Admins,
	array<string> Trolls
} file

void function OnStartInit()
{
	LoadAdminList()
	LoadTrollList()
}

void function LoadAdminList()
{
	string cvar = GetConVarString("admins")

	file.Admins = split(cvar, ",")

	foreach (string admin in file.Admins)
		StringReplace(admin, " ", "")
}

void function LoadTrollList()
{
	string cvar = GetConVarString("trolls")

	file.Trolls = split(cvar, ",")

	foreach (string player in file.Trolls)
		StringReplace(player, " ", "")
}

bool function PlayerIsAdmin(entity player)
{
	return (
		file.Admins.contains(player.GetPlayerName().tolower())
		|| file.Admins.contains(player.GetPlayerName())
		|| file.Admins.contains(player.GetUID())
	)
}

bool function PlayerIsTroll(entity player)
{
	return (
		file.Trolls.contains(player.GetPlayerName().tolower())
		|| file.Trolls.contains(player.GetPlayerName())
		|| file.Trolls.contains(player.GetUID())
	)
}
