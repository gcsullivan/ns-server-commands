global function AutoEjectHook

void function AutoEjectHook()
{
	AddCallback_GameStateEnter(eGameState.Playing, AutoEject)
}

void function AutoEject()
{
    if (GetConVarInt("auto_eject") < 1)
        return

    if (GAMETYPE != "lts")
        return

    thread EjectPlayers()
}

void function EjectPlayers()
{
    wait 5

    int allowedTitanCount = GetAllowedTitanCount()

    array<entity> team1 = []
    array<entity> team2 = []

    foreach (entity player in GetPlayerArray())
    {
        bool hasTitan = player.IsTitan() || (player.GetPetTitan() != null)

        if (!IsAlive(player) || !hasTitan)
            continue

        if (player.GetTeam() == TEAM_IMC)
            team1.append(player)
        else
            team2.append(player)
    }

    array<entity> chosenPlayers = []

    while (team1.len() > allowedTitanCount)
    {
        int randomIndex = RandomInt(team1.len())

        entity player = team1.remove(randomIndex)

        chosenPlayers.append(player)
    }

    while (team2.len() > allowedTitanCount)
    {
        int randomIndex = RandomInt(team2.len())

        entity player = team2.remove(randomIndex)

        chosenPlayers.append(player)
    }

    foreach (entity player in chosenPlayers)
    {
        if (!player.IsTitan())
        {
            entity titan = GetPlayerTitanInMap(player)

            if (titan != null)
                titan.Die(null, null, { damageSourceId = eDamageSourceId.damagedef_suicide })
        }

        thread Slay([player])
    }
}

int function GetAllowedTitanCount()
{
    int allowedTitanCount = 1

    array<entity> team1 = []
    array<entity> team2 = []

    foreach (entity player in GetPlayerArray())
    {
        if (!IsAlive(player))
            continue

        if (player.GetTeam() == TEAM_IMC)
            team1.append(player)
        else
            team2.append(player)
    }

    int lowestPlayerCount = ( team1.len() < team2.len() ) ? team1.len() : team2.len()

    if (lowestPlayerCount > 0)
        allowedTitanCount = lowestPlayerCount

    return allowedTitanCount
}
