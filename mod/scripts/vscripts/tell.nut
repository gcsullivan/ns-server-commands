global function TellCommand

struct {
    int r = 255
    int g = 255
    int b = 0
    string message
} settings

void function TellCommand()
{
    #if SERVER

    AddClientCommandCallback("tell", Command)

    UpdateMessageInfo()

    AddCallback_GameStateEnter(eGameState.Playing, AutoMessage)

    #endif
}

void function UpdateMessageInfo()
{
	array<string> rgb = split(GetConVarString("tell_color"), ",")
	foreach (string color in rgb)
		StringReplace(color, " ", "")

    settings.r = rgb[0].tointeger()
    settings.b = rgb[1].tointeger()
    settings.g = rgb[2].tointeger()

    settings.message = GetConVarString("tell_message")
}

void function AutoMessage()
{
    if (settings.message.len() > 0)
	{
		wait 3

        Tell(GetPlayerArray(), settings.message)
	}
}

bool function Command(entity player, array<string> args)
{
	if (!PlayerIsAdmin(player))
	{
		return true
	}

	if (args.len() < 2)
	{
		print("Usage: tell <player> <message>")
		return true
	}

	array<entity> players = GetSelectedPlayers(player, [args[0]])

	string message = ""
	foreach (string word in args.slice(1))
		message += word + " "

	thread Tell(players, message)

    return true
}

void function Tell(array<entity> players, string message)
{
    foreach (entity player in players)
	{
		if (player == null)
			continue

        SendHudMessage(player, message, -1, 0.4, settings.r, settings.b, settings.g, 0, 0.15, 4, 0.15)
	}
}
