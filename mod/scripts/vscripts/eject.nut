global function EjectCommand
global function Eject

void function EjectCommand()
{
	#if SERVER
	AddClientCommandCallback("eject", Command)
	#endif
}

bool function Command(entity player, array<string> args)
{
	if (PlayerIsAdmin(player))
	{
		thread Eject(GetSelectedPlayers(player, args))
	}

	return true
}

void function Eject(array<entity> players)
{
	foreach (entity player in players)
	{
		if (player == null || !IsAlive(player) || !player.IsTitan())
			continue

		TitanEjectPlayer(player)
	}
}
